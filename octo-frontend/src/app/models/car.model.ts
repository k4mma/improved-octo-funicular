export class Car {
  id: string;
  name: string;
  capacity: number;
  speed: number;
  fromHour: Date;
  toHour: Date;
  workingDays: string[];
}

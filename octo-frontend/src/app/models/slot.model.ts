export class Slot{
  id: string;
  carId: string;
  orderId: string;
  start: Date;
  end: Date;
}

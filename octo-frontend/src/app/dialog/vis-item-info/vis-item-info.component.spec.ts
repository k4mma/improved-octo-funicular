import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisItemInfoComponent } from './vis-item-info.component';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';

describe('VisItemInfoComponent', () => {
  let component: VisItemInfoComponent;
  let fixture: ComponentFixture<VisItemInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [VisItemInfoComponent, MatDialogModule],
      providers: [{ provide: MatDialogRef, useValue: {} }, { provide: MAT_DIALOG_DATA, useValue: {} }]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VisItemInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { Slot } from '../../models/slot.model';

@Component({
  selector: 'kma-vis-item-info',
  standalone: true,
  imports: [MatDialogModule],
  templateUrl: './vis-item-info.component.html',
  styleUrl: './vis-item-info.component.scss'
})
export class VisItemInfoComponent {

  constructor(
    public dialogRef: MatDialogRef<VisItemInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public slot: Slot & { carName: string }
  ) { }

}

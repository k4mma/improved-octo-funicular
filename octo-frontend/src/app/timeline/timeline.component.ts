/* eslint-disable no-extra-boolean-cast */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { take } from 'rxjs';
import { DataSet } from 'vis-data';
import { DataSetDataGroup, DataSetDataItem, DateType, Timeline, TimelineOptions } from 'vis-timeline';
import { DATE_FORMATS } from '../app.config';
import { VisItemInfoComponent } from '../dialog/vis-item-info/vis-item-info.component';
import { AppService } from '../service/app.service';
import { EventService } from '../service/event.service';

@Component({
  selector: 'kma-timeline',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: DATE_FORMATS }
  ],
  templateUrl: './timeline.component.html',
  styleUrl: './timeline.component.scss'
})
export class TimelineComponent implements OnInit, AfterViewInit {
  private readonly hoverBarId = "hoverBarId";
  private eventSource: EventSource
  id = 'test'
  timeline: Timeline;

  date = new FormControl(new Date());

  visTimelineItems: DataSetDataItem = new DataSet({});
  visTimelineGroups: DataSetDataGroup = new DataSet({});
  private now = new Date()
  hasMoved = false;


  opts: TimelineOptions = {
    start: new Date(this.now).setHours(this.now.getHours() - 1),
    end: new Date(this.now).setHours(this.now.getHours() + 3),
    width: "100%",
    height: "100%",
    // maxHeight: 150,
    minHeight: '80vh',
    moveable: true,
    multiselect: false,
    orientation: {
      axis: "top",
      item: "top"
    },
    editable: {
      updateTime: false,
      remove: true,
      add: false
    },
    onRemove: (item, callback) => {
      this.appService.deleteSlotById(item.id as string)
        .pipe(take(1))
        .subscribe()

    },
    selectable: true,
    verticalScroll: true,
    horizontalScroll: true,
    zoomKey: 'shiftKey',
    showCurrentTime: true,
    configure: false,
    rollingMode: {
      follow: true,
      offset: 0.25
    }
  };

  constructor(
    private appService: AppService,
    private eventService: EventService,
    private dialog: MatDialog) {
    this.eventSource = this.eventService.eventSource
  }

  ngOnInit(): void {
    this.date.valueChanges.subscribe(v => {
      this.move(v)
    })

    this.appService.getCars().pipe(take(1)).subscribe({
      next: cars => {
        this.visTimelineGroups.add(cars.map(c => { return { id: c.id, content: c.name }; }))
        this.visTimelineGroups.add([{id: 'null', content: ''}])
      }
    })

    this.appService.getPlanning().pipe(take(1)).subscribe({
      next: slots => {
        this.visTimelineItems.add(slots.map(s => { return { id: (!!s.id ? s.id:'null'), group: s.carId, content: s.orderId, start: s.start, end: s.end } }));
      }
    })

    this.eventSource?.addEventListener("car", (event) => {
      const data = JSON.parse(event.data)
      switch (data.action) {
        case "CREATE": {
          const c = JSON.parse(data.message);
          this.visTimelineGroups.add({ id: c.id, content: c.name })
          break;
        }
        case "DELETE":
          this.visTimelineGroups.remove(data.message)
          break;
      }
    })

    this.eventSource?.addEventListener("slot", (event) => {
      const data = JSON.parse(event.data)
      switch (data.action) {
        case "CREATE": {
          const s = JSON.parse(data.message);
          this.visTimelineItems.add({ id: s.id, group: s.carId, content: s.orderId, start: s.start, end: s.end })
          break;
        }
        case "UPDATE": {
          break;
        }
        case "DELETE": {
          this.visTimelineItems.remove(data.message);
        }
      }
    })


  }

  ngAfterViewInit(): void {
    const container = document.getElementById('timeline')
    this.timeline = new Timeline(container, this.visTimelineItems, this.visTimelineGroups, this.opts)
    this.timeline.setData({ groups: this.visTimelineGroups, items: this.visTimelineItems })
    this.timeline.on('rangechanged', (_) => {
      this.hasMoved = true
    })
    document.getElementsByClassName('vis-vertical')
    this.timeline.on('click', (e) => {
      if (e.event.shiftKey) {
        try {
          this.timeline.removeCustomTime(this.hoverBarId)
        } catch (error) {
          // ignore
        }
        this.timeline.addCustomTime(e.time, this.hoverBarId)
      }
    });
    this.timeline.on('select', (e) => {
      if (e.event['changedPointers'][0]['ctrlKey']) {
        const item = this.visTimelineItems.stream().filter(item => item.id === e.items[0]).toItemArray()[0]
        const carName = this.visTimelineGroups.stream().filter(car => car.id === item.group).toItemArray()[0].content
        this.appService.getSlotById(item.id.toString()).pipe(take(1)).subscribe({
          next: (slot) => {
            this.dialog.open(VisItemInfoComponent, { data: { ...slot, carName }, width: '50%' })
          }
        })
      }
    })
  }

  revert() {
    this.timeline.toggleRollingMode()
  }

  move(date: DateType) {
    if (!this.hasMoved)
      this.timeline.toggleRollingMode()
    this.timeline.moveTo(date)
    this.hasMoved = true
  }

}

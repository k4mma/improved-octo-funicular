import { CommonModule } from '@angular/common';
import { Component, OnDestroy } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { v4 as uuidv4 } from 'uuid';
import { TimelineComponent } from './timeline/timeline.component';
import { EventService } from './service/event.service';

@Component({
  selector: 'kma-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    TimelineComponent
  ],

})
export class AppComponent implements OnDestroy {
  title = 'octo-frontend'

  public constructor(
    private cookieService: CookieService,
    private eventService: EventService
  ) {
    if(!cookieService.check("SESSION_ID")){
      const uuid = uuidv4()
      cookieService.set("SESSION_ID", uuid, 2)
    }
    this.eventService.eventHandler()
  }

  ngOnDestroy(): void {
    this.cookieService.delete("SESSION_ID");
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Car } from '../models/car.model';
import { Slot } from '../models/slot.model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  readonly planningUrl = "http://localhost:9193/rest/api/plannings/v1"

  constructor(
    private http: HttpClient
  ) { }

  getCars(): Observable<Car[]> {
    return this.http.get<Car[]>("http://localhost:9192/rest/api/cars/v1");
  }

  getPlanning(): Observable<Slot[]> {
    return this.http.get<Slot[]>(this.planningUrl);
  }

  getSlotById(slotId: string): Observable<Slot> {
    return this.http.get<Slot>(`${this.planningUrl}/${slotId}`);
  }

  deleteSlotById(slotId: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.planningUrl}/${slotId}`);
  }
}

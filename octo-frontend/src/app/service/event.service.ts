import { Injectable, OnDestroy } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root'
})
export class EventService implements OnDestroy {
  readonly SSE_RECONNECT_UPPER_LIMIT = 64;
  eventSource: EventSource;
  reconnectFrequencySec: number;
  reconnectTimeout;

  constructor(
    private cookieService: CookieService,
    private appService: AppService
  ) { }

  eventHandler() {
    this.openSseChannel()
  }

  private openSseChannel(): boolean {
    this.createSseEventSource();
    return !!this.eventSource;
  }

  private createSseEventSource(): void {
    if (this.eventSource) {
      this.closeSseConnection();
      this.eventSource = null;
    }

    this.eventSource = new EventSource(`http://localhost:9191/rest/api/events/v1/?sessionId=${this.cookieService.get("SESSION_ID")}`)


    this.eventSource.onopen = () => {
      this.reconnectFrequencySec = 1;
    }

    this.eventSource.onerror = (_) => {
      this.reconnectOnError();
    }

    // delay to do it after sse creation
    // setTimeout(() => this.ping(), 2000)
  }


  private reconnectOnError() {
    const self = this;
    this.closeSseConnection()
    clearTimeout(this.reconnectTimeout);
    this.reconnectTimeout = setTimeout(() => {
      self.openSseChannel();
      self.reconnectFrequencySec *= 2
      console.log(self.reconnectFrequencySec);
      if (self.reconnectFrequencySec >= this.SSE_RECONNECT_UPPER_LIMIT) {
        self.reconnectFrequencySec = this.SSE_RECONNECT_UPPER_LIMIT;
      }
    }, this.reconnectFrequencySec * 1000)
  }

  private closeSseConnection() {
    this.eventSource.close()
  }

  ngOnDestroy(): void {
    this.eventSource?.close()
  }

}

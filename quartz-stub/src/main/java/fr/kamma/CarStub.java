package fr.kamma;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import io.quarkus.runtime.StartupEvent;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import net.bytebuddy.utility.RandomString;

@Path("/")
@ApplicationScoped
public class CarStub {
    private static final Logger log = Logger.getLogger(CarStub.class);
    private List<Car> cars = new ArrayList<>();
    private Random rdm = new Random();

    @Inject
    @RestClient
    CarClient client;

    void initSomeData(@Observes StartupEvent ev) {
        for (int i = 0; i < 5; i++) {
            cars.add(client.create(createRandomCar()));
        }
    }

    @Transactional
    @Scheduled(every = "5s")
    void createCar() {
        if (rdm.nextInt(0, 10) >= 6) {
            try {
                int index = rdm.nextInt(0, cars.size());
                if (client.delete(cars.get(index).id))
                    cars.remove(index);
            } catch (IllegalArgumentException e) {
                // ignore
            }
        } else
            cars.add(client.create(createRandomCar()));
    }

    private Car createRandomCar() {
        var name = RandomString.make(10);
        var capacity = rdm.nextInt(10, 100);
        var speed = rdm.nextInt(30, 100);
        var l1 = rdm.nextLong(0, 86400);
        var l2 = rdm.nextLong(0, 86400);
        var fromTime = LocalTime.ofSecondOfDay(Math.min(l1, l2));
        var toTime = LocalTime.ofSecondOfDay(Math.max(l1, l2));
        var workingDays = List.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY,
                DayOfWeek.FRIDAY);

        return new Car(name, capacity, speed, fromTime, toTime, workingDays);
    }

    @GET
    public List<Car> test() {
        return cars;
    }

    @RegisterRestClient(configKey = "car")
    interface CarClient {
        @POST
        @Path("/")
        @Produces(MediaType.APPLICATION_JSON)
        Car create(Car car);

        @DELETE
        @Path("/{id}")
        Boolean delete(@PathParam("id") String id);
    }

    @RegisterForReflection
    static class Car {

        public String id;
        public String name;
        public int capacity;
        public float speed;
        public LocalTime fromHour;
        public LocalTime toHour;
        public List<DayOfWeek> workingDays;

        public Car() {
        }

        public Car(String name, int capacity, float speed, LocalTime fromHour, LocalTime toHour,
                List<DayOfWeek> workingDays) {
            this.name = name;
            this.capacity = capacity;
            this.speed = speed;
            this.fromHour = fromHour;
            this.toHour = toHour;
            this.workingDays = workingDays;
        }

    }

}

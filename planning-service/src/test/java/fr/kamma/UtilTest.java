package fr.kamma;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.junit.jupiter.api.Test;

public class UtilTest {

    @Test
    void testIsIncludedIn() {
        var t1 = LocalTime.of(22, 0, 0);
        var t2 = LocalTime.of(9, 0, 0);
        var t3 = LocalTime.of(16, 0, 0);
        var t4 = LocalTime.of(12, 0, 0);
        var d = LocalDate.of(2222, 2, 2);

        assertTrue(Util.isIncludedIn(LocalDateTime.of(d, t4), LocalDateTime.of(d, t3), t2, t1));
        assertFalse(Util.isIncludedIn(LocalDateTime.of(d, t2), LocalDateTime.of(d, t3), t4, t1));
        assertFalse(Util.isIncludedIn(LocalDateTime.of(d, t4), LocalDateTime.of(d, t1), t2, t3));
        assertFalse(Util.isIncludedIn(LocalDateTime.of(d, t2), LocalDateTime.of(d, t1), t4, t3));
    }
}

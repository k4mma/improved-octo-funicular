package fr.kamma;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import fr.kamma.models.Car;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@RegisterRestClient(configKey = "car")
public interface CarClient {

    @GET
    @Path("/")
    List<Car> getCars();
    
}

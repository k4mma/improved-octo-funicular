package fr.kamma;

import static com.mongodb.client.model.Filters.eq;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.bson.types.ObjectId;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse.StatusCode;

import fr.kamma.models.Car;
import fr.kamma.models.Slot;
import io.quarkus.panache.common.Sort;
import io.quarkus.runtime.Startup;
import io.vertx.core.impl.ConcurrentHashSet;
import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/")
@ApplicationScoped
public class PlanningServiceResource {
    private static final Logger log = Logger.getLogger(PlanningServiceResource.class);
    private static final ZoneOffset UTC = ZoneOffset.UTC;

    List<Car> cars = Collections.synchronizedList(new ArrayList<>());

    @Inject
    EventService eventService;

    @RestClient
    @Inject
    CarClient carClient;

    @Startup
    void listenToEvent() {
        cars.addAll(carClient.getCars());
        eventService.streamCars()
                .onFailure().retry().withBackOff(Duration.ofSeconds(2), Duration.ofSeconds(20)).indefinitely()
                .subscribe().with(e -> {
                    switch (e.action()) {
                        case "DELETE":
                            cars.removeIf(c -> c.id().equals(e.message()));
                            break;
                        case "CREATE":
                            cars.add(new JsonObject(e.message()).mapTo(Car.class));
                            break;
                        default:
                            log.error("Not mapped action : %s".formatted(e.action()));
                            break;
                    }
                    log.info(cars.size());
                },
                        failure -> log.error(failure),
                        () -> log.warn("Complete"));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Slot> getSlots() {
        return Slot.findAll().list();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Slot getSlotById(@PathParam("id") String slotId) {
        return Slot.<Slot>findById(new ObjectId(slotId));
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public boolean deleteSlotById(@PathParam("id") String slotId){
        var result = Slot.deleteById(new ObjectId(slotId));
        if(result){
            eventService.postEvent("slot", null, StatusCode.OK, "DELETE", slotId);
        }
        return result;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postCommand(Command command) {
        var item = Slot.<Slot>mongoCollection().find(eq("orderId", command.id())).first();
        var isNewItem = item == null;

        if (isNewItem) {
            item = new Slot(command.id());
        }

        var distance = Math.sqrt(
                Math.pow(command.toPoint().x() - command.fromPoint().x(), 2)
                        + Math.pow(command.toPoint().y() - command.fromPoint().y(), 2));

        var availableSlot = cars.parallelStream()
                .map(c -> getAvailableSlot(c, distance, command))
                .filter(s -> s != null)
                .sorted((s1, s2) -> s1.start.compareTo(s2.start))
                .findFirst()
                .orElse(new Slot(null, command.from(), command.to()));

        item.populate(availableSlot);

        item.persistOrUpdate();
        eventService.postEvent("slot", null, StatusCode.CREATED, isNewItem ? "CREATE" : "UPDATE",
                JsonObject.mapFrom(item).encode());
    }

    Slot getAvailableSlot(Car car, double distance, Command command) {
        var commandTime = distance * 4800 / car.speed();
        var startDate = command.from().toLocalDate();
        var endDate = command.to().toLocalDate();
        var nbrOfDays = (int) endDate.toEpochDay() - startDate.toEpochDay() + 1;

        Set<Slot> carSlots = new ConcurrentHashSet<Slot>();
        for (int index = 0; index < nbrOfDays; index++) {
            carSlots.add(new Slot(null, LocalDateTime.of(startDate.plusDays(index), LocalTime.ofSecondOfDay(0)),
                    car.fromHour().atDate(startDate.plusDays(index))));
            carSlots.add(new Slot(null, car.toHour().atDate(startDate.plusDays(index)),
                    LocalDateTime.of(startDate.plusDays(index), LocalTime.ofSecondOfDay(86399L))));
        }
        carSlots.addAll(Slot.list("carId", Sort.ascending("start"), car.id()));
        var filteredcarSlots = carSlots.stream()
                .sorted((slot0, slot1) -> slot0.start.compareTo(slot1.start))
                .collect(Collectors.toList());
                
        for (int i = 0; i < filteredcarSlots.size() - 1; i++) {
            var fsStart = filteredcarSlots.get(i).end;
            var fsEnd = filteredcarSlots.get(i+1).start;
            var freeSlotDuration = fsEnd.toEpochSecond(UTC) - fsStart.toEpochSecond(UTC);
            if (freeSlotDuration >= commandTime) {
                var fdsStart = fsStart.isBefore(command.from()) ? command.from():fsStart;
                var fdsEnd = fdsStart.plusSeconds((long) commandTime);
                if (fdsEnd.isBefore(fsEnd) && fdsEnd.isBefore(command.to())){
                    return new Slot(car.id(), fdsStart, fdsStart.plusSeconds((long) commandTime));
                }
            }
        }
        return null;
    };

    record Command(String id, LocalDateTime from, LocalDateTime to, Point fromPoint, Point toPoint) {
    }

    record Point(int x, int y) {
    }

}
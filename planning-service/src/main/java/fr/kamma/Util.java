package fr.kamma;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public final class Util {

    private Util() {
    }

    public static final boolean isIncludedIn(LocalDateTime from, LocalDateTime to, LocalTime refFrom, LocalTime refTo) {
        var d = LocalDate.of(1, 1, 1);
        var d1 = from.toLocalTime().atDate(d);
        var d2 = to.toLocalTime().atDate(d);
        var d3 = refFrom.atDate(d);
        var d4 = refTo.atDate(d);

        return d1.isAfter(d3) && d1.isBefore(d4) && d2.isAfter(d3) && d2.isBefore(d4);
    }

    public static final boolean isIncludedIn(LocalDateTime from, LocalTime refFrom, LocalTime refTo) {
        var d = LocalDate.of(1, 1, 1);
        var d1 = from.toLocalTime().atDate(d);
        var d3 = refFrom.atDate(d);
        var d4 = refTo.atDate(d);

        return d1.isAfter(d3) && d1.isBefore(d4);
    }

    public static final boolean isIncludedIn(LocalDateTime from, LocalDateTime refFrom, LocalDateTime refTo) {
        var d1 = from;
        var d3 = refFrom;
        var d4 = refTo;

        return d1.isAfter(d3) && d1.isBefore(d4);
    }
}

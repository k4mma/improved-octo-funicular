package fr.kamma;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.reactive.client.SseEvent;
import org.jboss.resteasy.reactive.client.SseEventFilter;

import io.smallrye.mutiny.Multi;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@ApplicationScoped
public class EventService {

    @Inject
    @RestClient
    EventClient eventClient;

    public void postEvent(String name, String sessionId, Integer statusCode, String action, String message) {
        eventClient.postEvent(name, new EventData(sessionId, statusCode, action, message));
    }

    public Multi<EventData> streamCars() {
        return eventClient.streamEvents(Arrays.asList("heartbeat", "car"))
                .map(e -> e.data());
    }

    @RegisterRestClient(configKey = "event")
    interface EventClient {
        @Path("/{name}")
        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        void postEvent(@PathParam("name") String name, EventData eventData);

        @GET
        @Path("/")
        @Produces(MediaType.SERVER_SENT_EVENTS)
        @SseEventFilter(HeartbeatFilter.class)
        Multi<SseEvent<EventData>> streamEvents(@QueryParam("name") List<String> names);

        class HeartbeatFilter implements Predicate<SseEvent<String>> {
            @Override
            public boolean test(SseEvent<String> event) {
                return !"heartbeat".equals(event.name());
            }

        }
    }

    record EventData(String sessionId, Integer statusCode, String action, String message) {
    }

}

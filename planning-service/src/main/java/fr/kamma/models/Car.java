package fr.kamma.models;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

public record Car(String id, int capacity, int speed, LocalTime fromHour, LocalTime toHour,
                List<DayOfWeek> workingDays) {
}

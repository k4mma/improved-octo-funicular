package fr.kamma.models;

import java.time.LocalDateTime;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Slot extends PanacheMongoEntity {

    public LocalDateTime start;
    public LocalDateTime end;
    public String carId;
    public String orderId;
    public boolean fixed;

    public Slot() {
    }

    public Slot(String orderId) {
        this.orderId = orderId;
    }

    public Slot(String carId, LocalDateTime start, LocalDateTime end){
        this.start = start;
        this.end = end;
        this.carId = carId;
    }

    public void populate(Slot availableSlot) {
        this.carId = availableSlot.carId;
        this.start = availableSlot.start;
        this.end = availableSlot.end;
    }
}

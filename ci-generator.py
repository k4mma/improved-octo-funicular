from os import listdir
from pathlib import Path

def main():
    with open("child_pipeline.yml", "w") as f_out:
        f_out.write("""
default:
    tags:
        - rockamma
stages:
    - child-run
""")
        for dir in listdir('.'):
            path = Path(f'{dir}/.gitlab-ci.yml')
            if path.exists():
                f_out.write(f"""
{dir}:
    stage: child-run
    trigger:
        include: {str(path)}
        strategy: depend
    only:
        changes:
            - {dir}/**/*
            - .gitlab-ci.yml
""")
            
        

if __name__ == "__main__":
    main()
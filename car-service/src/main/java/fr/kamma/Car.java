package fr.kamma;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;

import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntity;
import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class Car extends ReactivePanacheMongoEntity {

    public String name;
    public int capacity;
    public int speed;
    public LocalTime fromHour;
    public LocalTime toHour;
    public List<DayOfWeek> workingDays;

}

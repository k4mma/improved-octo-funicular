package fr.kamma;

import java.util.List;

import org.bson.types.ObjectId;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.resteasy.reactive.RestResponse.StatusCode;

import io.smallrye.mutiny.Uni;
import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/")
@ApplicationScoped
public class CarResource {

    @Inject
    EventService eventService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<List<Car>> getAll() {
        return Car.listAll();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Car> getById(String id) {
        return Car.findById(new ObjectId(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Uni<Car> create(Car car) {
        return car.persist().onItem().castTo(Car.class)
                .call(c -> eventService.postEvent("car", null, StatusCode.CREATED, "CREATE",
                        JsonObject.mapFrom(car).encode()));
    }

    @DELETE
    @Path("/{id}")
    @APIResponse(responseCode = "200", description = "Successfully deleted")
    @APIResponse(responseCode = "404", description = "Entity not found")
    public Uni<Boolean> deleteById(@QueryParam("sessionId") String sessionId, @PathParam("id") String id) {
        return Car.deleteById(new ObjectId(id))
                .onItem().call(b -> {
                    if (b) {
                        return eventService.postEvent("car", null, StatusCode.NO_CONTENT, "DELETE", id);
                    }
                    return eventService.postEvent("error", sessionId, StatusCode.NOT_FOUND, "DELETE", id);
                })
                .invoke(b -> {
                    if (!b) {
                        throw new NotFoundException(id);
                    }
                });
    }

}

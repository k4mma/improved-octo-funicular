package fr.kamma;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;

@ApplicationScoped
public class EventService {

    @Inject
    @RestClient
    EventClient eventClient;

    public Uni<Void> postEvent(String name, String sessionId, Integer statusCode, String action, String message) {
        return eventClient.postEvent(name, new EventData(sessionId, statusCode, action, message));
    }

    @RegisterRestClient(configKey = "event")
    interface EventClient {
        @Path("/{name}")
        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        Uni<Void> postEvent(@PathParam("name") String name, EventData eventData);

    }

    record EventData(String sessionId, Integer statusCode, String action, String message) {
    }
}

package fr.kamma;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.OnOverflow;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestStreamElementType;

import io.smallrye.mutiny.Multi;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.sse.OutboundSseEvent;
import jakarta.ws.rs.sse.Sse;

@Path("/")
@ApplicationScoped
public class EventResource {

    private static final String HEARTBEAT = "heartbeat";

    private static final Logger log = Logger.getLogger(EventResource.class);

    @PostConstruct
    void keepAlive() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                var now = String.valueOf(new Date().getTime());
                eventsEmitter
                        .send(new Event(HEARTBEAT, new EventData(null, 200, HEARTBEAT, now)));
            }
        }, 0, 50000);
    }

    @PreDestroy
    void cleanup() {
        log.warn("I'm dying");
        eventsEmitter.complete();
    }

    @Inject
    @Channel("events")
    @OnOverflow(value = OnOverflow.Strategy.DROP)
    Emitter<Event> eventsEmitter;

    @Inject
    @Channel("events")
    Multi<Event> events;

    @Inject
    Sse sse;

    @GET
    @RestStreamElementType(MediaType.APPLICATION_JSON)
    public Multi<OutboundSseEvent> streamEvents(@QueryParam("sessionId") String sessionId,
            @QueryParam("name") List<String> names) {
        log.info("New connection id " + sessionId);
        var broadcast = events.broadcast().toAllSubscribers();
        var snames = names.stream().flatMap(n -> Arrays.stream(n.split(","))).toList();
        
        var nullIdStream = broadcast.filter(e -> e.data().sessionId() == null || e.data().sessionId().isBlank())
                .filter(e -> snames.isEmpty() || snames.contains(e.name()))
                .map(e -> sse.newEventBuilder().name(e.name()).data(e.data()).build());

        if (sessionId != null) {
            var sessionIdStream = broadcast
                    .filter(e -> snames.isEmpty() || snames.contains(e.name()))
                    .filter(e -> (e.data().sessionId() != null && !e.data().sessionId().isBlank())
                            ? e.data().sessionId().equals(sessionId)
                            : false)
                    .map(e -> sse.newEventBuilder().name(e.name()).data(e.data()).build());

            return Multi.createBy().merging().streams(sessionIdStream, nullIdStream);
        }
        return nullIdStream;
    }

    @GET
    @Path("/ping")
    public void ping(@QueryParam("sessionId") String sessionId) {
        eventsEmitter.send(new Event("ping", new EventData(sessionId, 200, "ping", "pong")));
    }

    @Path("/{name}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postEvent(@PathParam("name") String name, EventData data) {
        eventsEmitter.send(new Event(name, data));
    }

    record EventData(String sessionId, Integer statusCode, String action, String message) {
    }

    record Event(String name, EventData data) {
    }
}